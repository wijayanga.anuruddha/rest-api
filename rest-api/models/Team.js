const mongoose = require ('mongoose')

const Team = new mongoose.Schema({
	team :{type: String},
	city :{type: String}
})

module.exports = mongoose.model('Team', Team)