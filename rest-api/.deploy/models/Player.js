const mongoose = require ('mongoose')

const Player = new mongoose.Schema({
	firstname :{type: String},
	lastname :{type: String},
	team :{type: String}
})

module.exports = mongoose.model('Player', Player)